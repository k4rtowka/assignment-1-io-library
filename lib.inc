;Последний коммит с GitLab

section .data
newLine: db 0xA
%define syscallForExit 60  
%define syscallWrite 1
%define dStdout 1

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, syscallForExit 
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .counter:
        cmp [rdi+rax], byte 0
        je .end
        inc rax
        jmp .counter
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, syscallWrite
    mov rdi, dStdout
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, syscallWrite
    mov rdi, dStdout
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, newLine
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r8
    push r12
    mov rax, rdi
    mov r8, 10
    mov r12, rsp
    sub rsp, 24
    
    dec r12 
    mov byte [r12], 0

    .loop:
        xor rdx, rdx
        div r8
        add rdx, '0'
        dec r12
        mov byte[r12], dl
        test rax, rax
        jnz .loop
    
    mov rdi, r12
    call print_string

    add rsp, 24
    pop r12
    pop r8
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .print
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .print:
        jmp print_uint 

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx                   

    .loop:
        mov al, byte [rdi + rcx]     
        cmp al, byte [rsi + rcx]     
        jne .no               

        test al, al
        jz .yes             

        inc rcx                 
        jmp .loop

    .yes:
        mov rax, 1              
        ret

    .no:
        xor rax, rax            
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    sub rsp, 8
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall

    test rax, rax
    jle .eofOrError

    mov al, [rsp]
    jmp .end
    
    .eofOrError:
        xor rax, rax
    
    .end:
        add rsp, 8
        ret  



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word: 
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14

    .read
        call read_char

        cmp rax, ' '
        jz .space
        cmp rax, `\t`
        jz .space
        cmp rax, `\n`
        jz .space

        cmp r14, r13
        je .error

        test al, al
	    je .finish

        mov byte [r12 + r14], al
        inc r14

        jmp .read

    .finish:
        mov byte [r12 + r14], 0
        mov rax, r12
	    mov rdx, r14
        jmp .end
    
    .space:
        test r14, r14
        jz .read
        jmp .finish

    .error:
        xor rax, rax    

    .end:
        pop r14
        pop r13
        pop r12
        ret


 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax       
    xor r8, r8       
    xor r9, r9      

    .num:
        mov r8b, byte [rdi+r9]  
        sub r8b, '0'             

        cmp r8b, 9              
        ja .end                

        imul rax, rax, 10       
        add rax, r8            
        inc r9                 
        jmp .num               

    .end:
        mov rdx, r9            
        ret    
       







; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
     mov al, byte[rdi]
     cmp al, '-'
     je .minus
     cmp al, '+'
     je .plus
     jmp parse_uint
     .minus:
          inc rdi
          call parse_uint
          inc rdx
          neg rax
          jmp .end
     .plus:
          inc rdi
          call parse_uint
          inc rdx
    .end:
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax

.loop:
	cmp rax, rdx
	jge .bad
	mov r9b, byte[rdi + rax]
	mov byte[rsi + rax], r9b
	inc rax
	test r9b, r9b
	je .end
	jmp .loop
.bad:	
	xor rax, rax
.end:
	ret 

